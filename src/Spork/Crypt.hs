--Crypt is the central module to spork where it handles the arguments and
--flags given to spork and directs the information to where it needs to go

module Spork.Crypt where

import System.Environment
import Data.List
import Control.Monad

import qualified Spork.Bridge as SBR
import qualified Spork.Exchange as SXC
import qualified Spork.Common.Batch as SCB
import qualified Spork.Algorithms.Caesar as SAC
import qualified Spork.Algorithms.Vigenere as SAV
import qualified Spork.Algorithms.Autokey as SAA
import qualified Spork.Algorithms.Hill as SAH
import qualified Spork.Algorithms.Railfence as SAR

process :: IO ()
process = do
     action:method:file:flags <- getArgs
     contents <- SBR.retrieve file
     let target = if path >= 0 then (drop 9 (flags !! path)) else file
                 where path = SXC.position "--output=" flags
     key <- SXC.grabkey flags
     when (elem "--create-keyfile" flags) (SXC.forgekey flags)
     SBR.write target (select action method key contents)

--select constructs a set of operations to perform on the contents of the file to be encrypted
--this is necessary as when double layering, the order of which operations are done is very important

select :: String -> String -> String -> (String -> String)
select strat cipher pass
    | take 6 cipher == "double" = select strat m2 k2 . select strat m1 k1
    | otherwise =
        case cipher of
                "caesar" -> SAC.shift (if strat == "encrypt" then (read pass) else (0-(read pass)))
                "vigenere" -> SAV.muddle pass (if strat == "encrypt" then (+) else (-))
                "autokey" -> (if strat == "encrypt" then SAA.muddle else SAA.unmuddle) pass
                "hill" -> (if strat == "encrypt" then SAH.encrypt else SAH.decrypt) pass
                "railfence" -> (if strat == "encrypt" then SAR.rail else SAR.derail) (read pass)
                _ -> errorx "invalid method"
        where {
            (m1,m2) = invert (strat == "decrypt") $ parse (drop 6 cipher) ',';
            (k1,k2) = invert (strat == "decrypt") $ parse pass ','
            }

invert False (a,b) = (a,b)
invert True (a,b) = (b,a)

--parse (along with count) are used to extract important information when double layering

parse :: String -> Char -> (String, String)
parse str ch = (tail $ intercalate [ch] (take int arr), init $ intercalate [ch] (drop int arr))
        where {
              int = count ch str (0,0);
              arr = SCB.split ch str
              }

count :: Integral a => Char -> String -> (a,a) -> a
count _ (x:[]) (_,_) = 1
count c (x:xs) (i,j)
  | i == 1 && x == c = j+1
  | x == '[' = count c xs (i+1,j)
  | x == ']' = count c xs (i-1,j)
  | x == c = count c xs (i,j+1)
  | otherwise = count c xs (i,j)

errorx msg = errorWithoutStackTrace $ "error => " ++ msg
