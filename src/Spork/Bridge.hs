--Bridge is a module used to operate and handle the files
--that will be going in and out of spork.

module Spork.Bridge where

import System.IO
import System.Directory
import Control.Exception
import Control.Monad

--in retrieve, opening a binary file means that we can open any file regardless
--encoding, and operate on it without problem. The String needs to be evaluated
--due to Haskell's Laziness.

retrieve :: String -> IO String
retrieve path = do
    confirm <- doesFileExist path
    when (confirm /= True) (errorx "file does not exist")
    handle <- openBinaryFile path ReadMode
    contents <- hGetContents handle
    evaluate (length contents)
    hClose handle
    return contents

write :: String -> String -> IO ()
write path pack = do
    handle <- openBinaryFile path WriteMode
    hPutStr handle pack
    hClose handle
    return ()

errorx msg = errorWithoutStackTrace $ "error => " ++ msg
