--Batch is a module containg useful list operations that can be used
--by many other modules within spork

module Spork.Common.Batch where

mapmat :: (a -> b) -> [[a]] -> [[b]]
mapmat f x = map (map f) x

maptuple :: (a -> b) -> (a, a) -> (b, b)
maptuple f (a1, a2) = (f a1, f a2)

swapitem :: Int -> Int -> [a] -> [a]
swapitem f s xs =
    zipWith (\x y ->
        if x == f then xs !! s
        else if x == s then xs !! f
        else y)
    [0..] xs

findpos :: Eq a => [a] -> a -> Int
findpos list elt = head [ index | (index, e) <- zip [0..] list, e == elt ]

absmax :: (Num a, Ord a) => [a] -> a  
absmax [x] = x  
absmax (x:x':xs) = absmax ((if abs x >= abs x' then x else x'):xs)

split :: Eq a => a -> [a] -> [[a]]
split d [] = []
split d s = x : split d (drop 1 y) where (x,y) = span (/= d) s
