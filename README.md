# Spork

Spork is a minimal, purely functional command-line tool for encryption, decryption and hashing, written entirely in Haskell. It's an endeavor I took on in order to explore my interest in learning Haskell and dipping my toes into functional programming and also to feed my love for mathematics.

## Features

Spork is aiming to support a vast array of symmetric key algorithms as of the latest commit, Spork supports:

- Caesar Cipher         {caesar}
- Vigenere Cipher       {vigenere}
- Autokey Cipher        {autokey}
- Hill Cipher           {hill}
- Rail Fence Cipher     {railfence}

Spork also features Double Layering, a method in which you apply one cipher and then another, in order to increase security. This can work with varying effectiveness depending on the cryptography methods used. The syntax with double layering is slightly different but should still be easy to grasp.

## Building
To get started with Spork you can simply navigate to the src folder and run the build script located inside. The only dependency is the ghc compiler, with that it should build fine and off we go!

Note: Haskell libraries are awfully complicated to handle on arch-based distros. As Spork uses minimal dependencies as long as you have ghc and ghc-static, you should have no problems.

```
cd src && ./build.sh
```

## Usage

Once built you can run the binary. Spork takes arguments as so:

```
Spork [action] [method] [file] [options]
```

### Options

`--key="STRING"` provide the key by which your file will be encrypted/decrypted, directly through a string.

`--keyfile="FILE"` provide the key by which your file will be encrypted/decrypted, where the key is stored in a file.

A key must be provided by one of these methods.
It is recommended you use a password generator if you would like to create a truly random, secure key.

`--output="PATH"` define a custom path to export your encrypted file to, instead of replacing the source file.

`--query` test other given options for their functionality, e.g. combined with `--key`, you can check which ciphers the key given will work with.

`--create-keyfile` creates a keyfile based on the key used in the encryption process.

### Important Notes

#### Keys

Not every cipher will accept every key, for example the Caesar Cipher can only take an integer as a key. This is why it is important to utilise the tstkey action before encrypting important data, it will run a series of tests on your key to check what algorithms are able to use it.
It is also important to consider the characters used in your key, as you will see, certain characters are used in the structure of usage for Spork, and these characters should not be used within your passwords. This also applies to characters that your shell recognises. Any alphabetical and numerical characters will work perfectly, just avoid characters such as '[' / ']', ',', ';' etc. 

## Philosophy

* __Readable and Refined Codebase__ - The codebase should always be easy to access and work upon, so that anyone can contribute. Haskell is already a language where code is quite beautiful and easily readable, provided you know the language.
* __Minimal Dependencies__ - While this requires more work in the short term, working with almost 0 dependencies allows bugs to be tackled at the source and properly taken care of. Far too many times have I seen maintainers shrug at issues and say "It's not me , it's upstream". How this will be accomplished is by reducing the dependencies of this project to simply the base package, which only contains the basic libraries for Haskell.
* __Purely Functional__ - As with the Haskell language itself, Spork is committing to being purely functional, where for any one input you will always get the exact same output (for that input). For this reason I have omitted a random password generator as this would breach this philosophy.
