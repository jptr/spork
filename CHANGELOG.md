# Changelog
All notable changes to spork will be documented here.

## [0.1.2] - 2020-08-11
### Added
- A full implementation of Double Layering, allowing you to apply multiple ciphers to a piece of text.
- Implemented the Rail Fence Transposition Cipher.
- The create-keyfile option, which allows you to store the key used for encryption and decryption into a file. (Helpful if you're using large keys)
### Changed
- Changed the system for interpreting arguments under the hood.
- Certain Modules were renamed, to make more sense and make the project easier to navigate.

## [0.1.1] - 2020-08-05
### Added
- Fully implement the Autokey Cipher, a cipher similar to the Vigenere Cipher.
- The query option, which allows you to test keys for functionality and will be used for more in the future.

## [0.1.0] - 2020-08-04
### Added
- The first implemented encryption methods, the Caesar Cipher, the Vigenere Cipher and the Hill Cipher.
- This CHANGELOG, to keep track of spork's development.
- A README, for a suitable summary of the application.
- An MIT license.
