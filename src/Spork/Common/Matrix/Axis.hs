--The Crux module contains the essential operations
--required to work with Matrices in the more
--specific Matrix modules

module Spork.Common.Matrix.Axis where

import Data.List

import qualified Spork.Common.Batch as SCB

type Vector a = [a]
type Matrix a = [[a]]

dotproduct :: Num a => Vector a -> Vector a -> a
dotproduct v1 v2 = sum (zipWith (*) v1 v2)

matrixproduct :: Num a => Matrix a -> Matrix a -> Matrix a
matrixproduct m1 m2 = [map (dotproduct row) (transpose m2) | row <- m1]

scalevector :: Num a => a -> Vector a -> Vector a
scalevector n vec = [n * x | x <- vec]

scalematrix :: Num a => a -> Matrix a -> Matrix a
scalematrix n mat = [scalevector n row | row <- mat]

modmatrix :: Integral a => Matrix a -> a -> Matrix a
modmatrix mat n = [ [ mod b n | b <- a ] | a <- mat ]

diagonal :: Matrix a -> [a]
diagonal xs = zipWith (!!) xs [0..]

identity :: Int -> Matrix Int
identity size = [ [ fromEnum $ x == y | y <- [1..size]] | x <- [1..size] ]
