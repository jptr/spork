--Hill is the module for the implementation of the Hill Cipher
--It utilises many of the functions defined for Matrix operations
--from the Transform module.

module Spork.Algorithms.Hill where

import qualified Spork.Common.Matrix.Axis as SMA
import qualified Spork.Common.Matrix.Transform as SMT

encrypt :: String -> String -> String
encrypt pass contents = SMT.unchunk [ SMA.modmatrix b 256 | b <- [ SMA.matrixproduct key a | a <- (SMT.marker (SMT.initial contents (length $ head key))) ] ]
    where key = map (map round) (SMT.keygen pass)

decrypt :: String -> String -> String
decrypt pass contents = SMT.unchunk $ SMT.unmarker [ SMA.modmatrix b 256 | b <- [ (SMA.matrixproduct unkey a) | a <-  (SMT.initial contents (length $ head unkey)) ] ]
    where unkey = SMT.modinverse $ (map (map round) (SMT.keygen pass))
