--Factor is a module I created, not only to boost the performance of spork
--but also out of sheer interest. In most libraries and programs that deal
--with matrices they use this method for calculating determinants due to its
--amazing speed in comparison to other methods.

module Spork.Common.Matrix.Factor where

import Data.List

import qualified Spork.Common.Batch as SCB
import qualified Spork.Common.Matrix.Axis as SMA

type Matrix a = [[a]]

--this method of computing a determinant is ridiculously faster than the other method
--described in the Transform module, with significantly less operations to perform

determinant :: (Fractional a, Ord a) => Matrix a -> a
determinant xs = (-1)^(snd $ fst chop) * (product (SMA.diagonal (snd $ snd chop)))
        where chop = lup xs

--to produce an LUP decomposition, a pivot matrix needs to be found and then used in combination
--with a normal LU decomposition to produce all the information required

lup :: (Fractional a, Ord a) => Matrix a -> ((Matrix a, Int), (Matrix a, Matrix a))
lup xs = (p, (l, u))
    where {
        p = pivot (xs, SCB.mapmat fromIntegral (SMA.identity $ length xs)) 0 0;
        (l,u) = lu $ SMA.matrixproduct (fst p) xs
          }

pivot :: (Fractional a, Ord a) => (Matrix a, Matrix a) -> Int -> Int -> (Matrix a, Int)
pivot (x,i) col swp =
    if length x - 1 == col then (i, swp)
    else pivot (SCB.maptuple (SCB.swapitem col (len+col)) (x,i)) (col+1) (swp + fromEnum (len /= 0))
        where {
            list = drop col ((transpose x)!!col);
            len = SCB.findpos list (SCB.absmax list)
              }

--these functions are all that are required to produce an LU decomposition
--however this is not effective for all matrices, so an LUP decomposition is required

lu :: Fractional a => Matrix a -> (Matrix a, Matrix a)
lu = unzip . map unzip . decompose

decompose :: Fractional a => Matrix a -> [[(a,a)]]
decompose [] = []
decompose ((f:fs):xs) = zip (1 : repeat 0) (f:fs) : zipWith (:) (map (\(y:_) -> (y / f, 0)) xs) (decompose $ map sub xs)
       where sub (z:zs) = zipWith (-) zs $ map (z / f *) fs
