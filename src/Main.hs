--------------------------------------------------------------------------------------
--- Welcome to spork's source code, a bit of a mess sure, this is my first project ---
--- in Haskell after all. If you see any bugs or better implementations, feel free ---
---        to create a pull request, I will gladly review it. Godspeed!            ---
--------------------------------------------------------------------------------------

module Main where

import System.Environment
import System.Exit

import qualified Spork.Crypt as SCR
import qualified Spork.Debug as SDB
import qualified Spork.Exchange as SXC

--the main here grabs the arguments and if supplied with an argument such as
--version or help it will follow through with scanFlag, otherwise it will
--continue as normal

main :: IO ()
main = do
    bunch <- getArgs
    if take 2 (head bunch) == "--" then scanflag bunch >> exit else SCR.process

--if a relevant flag is supplied, deal with it then exit the process

scanflag :: [String] -> IO ()
scanflag extra
  | elem "--query" extra && (SXC.position "--key" extra >= 0) = SDB.testkey extra >> exit
  | elem "--create-keyfile" extra && (SXC.position "--key" extra >= 0) = SXC.forgekey extra
  | elem "--version" extra = version >> exit
  | elem "--help" extra = usage >> exit
  | otherwise = usage >> exit

version = putStrLn "spork 0.1.2"
usage   = putStrLn "Usage: spork [action] [method] [file] [options]"
exit    = exitWith ExitSuccess
