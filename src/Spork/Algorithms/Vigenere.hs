--Vigenere is a module for the Vigenere Cipher.
--It is quite similar to the Caesar Cipher.

module Spork.Algorithms.Vigenere where

import Data.Char

muddle :: String -> (Int -> Int -> Int) -> String -> String
muddle key action contents = zipWith (shuffle action) (cycle key) contents

shuffle :: (Int -> Int -> Int) -> Char -> Char -> Char
shuffle op pass chnk = chr (((ord chnk) `op` (ord pass)) `mod` 256)
