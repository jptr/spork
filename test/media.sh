#!/bin/sh
DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )
cd $DIR/..

cryptArray=(
    "caesar"
    "vigenere"
    "autokey"
    "hill"
    "railfence"
    "double[vigenere,autokey]"
    )

dirArray=(
    "caesar"
    "vigenere"
    "autokey"
    "hill"
    "railfence"
    "double"
    )

keyArray=(
    "6"
    "mmmbop"
    "indig0"
    "theorise"
    "11"
    "[territory,claim]"
    )

let w=0

for index in ${!cryptArray[*]}
do
    ./src/spork encrypt ${cryptArray[$index]} "$DIR/input/snap.mp3" --key="${keyArray[$index]}" --output="$DIR/tmp"
    if [[ $(cmp --silent "$DIR/output/${dirArray[$index]}/snap.mp3" "$DIR/tmp" && echo "success" || echo "failure") == "success" ]]
        then
            let w++
            printf "Media: Encryption Test #%s passed. " $(( $index+1 ))
        else
            printf "Media: Encryption Test #%s failed. " $(( $index+1 ))
        fi

    ./src/spork decrypt ${cryptArray[$index]} "$DIR/tmp" --key="${keyArray[$index]}"
    if [[ $(cmp --silent "$DIR/input/snap.mp3" "$DIR/tmp" && echo "success" || echo "failure") == "success" ]]
        then
            let w++
            printf "Decryption Test #%s passed.\n" $(( $index+1 ))
        else
            printf "Decryption Test #%s failed.\n" $(( $index+1 ))
        fi
done

let num=$(( 2*${#cryptArray[*]} ))

printf "%s out of %d Media Tests passed \n" $w $num

