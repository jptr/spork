--Caesar is the module for the implementation of the Caesar Cipher.
--It is fairly self explanatory.

module Spork.Algorithms.Caesar where

shift :: Integer -> String -> String
shift num contents
    | num > 0 = shift (num-1) (map next contents)
    | num < 0 = shift (num+1) (map previous contents)
    | num == 0 = contents

previous :: Char -> Char
previous chnk
  | chnk == '\NUL' = '\255'
  | otherwise = pred chnk

next :: Char -> Char
next chnk
  | chnk == '\255' = '\NUL'
  | otherwise = succ chnk
