#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR
ghc -O2 -o "$DIR/spork" "$DIR/Main.hs" -j4 +RTS -A32m -RTS
shopt -s globstar
rm **/*.hi **/*.o
