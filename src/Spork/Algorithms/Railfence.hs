
module Spork.Algorithms.Railfence where

import Data.List
import Data.Ord

rail :: Integral a => a -> [b] -> [b]
rail x txt = map fst $ sortBy (comparing snd) (zip txt $ rows x)

derail :: Integral a => a -> [b] -> [b]
derail x txt = map fst $ sortBy (comparing snd) (zip txt (rail x [1..(length txt)]))

rows :: Integral a => a -> [a]
rows x = cycle ([1..x-1] ++ [x,x-1..2])
