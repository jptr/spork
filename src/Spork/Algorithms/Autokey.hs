--Autokey is a module for the Autokey Cipher.
--It is almost identical to the Vigenere Cipher, however
--instead of repeating the cipher key over the length of
--the cipher text, the contents itself are added on
--to the end of the key.

module Spork.Algorithms.Autokey where

import Data.Char

muddle :: String -> String -> String
muddle key contents = zipWith (shuffle (+)) (cycle (key++contents)) contents

unmuddle :: String -> String -> String 
unmuddle key contents = drop (length key) (reconstruct key contents)

reconstruct :: String -> String -> String
reconstruct x [] = x
reconstruct (x:xs) (y:ys) = x:(reconstruct ((xs)++[shuffle (-) x y]) ys)

shuffle :: (Int -> Int -> Int) -> Char -> Char -> Char
shuffle op pass chnk = chr (((ord chnk) `op` (ord pass)) `mod` 256)
