--Debug is a module that can be used both by users and developers to
--prod at spork and test if certain things will work. This is definitely
--useful in the case of keys as not all keys are usable for every cipher.

module Spork.Debug where

import System.Directory
import Control.Monad

import qualified Spork.Exchange as SXC
import qualified Spork.Common.Matrix.Transform as SMT
import qualified Spork.Common.Matrix.Factor as SMF

testkey :: [String] -> IO ()
testkey x = do
    key <- SXC.grabkey x
    putStr ("Caesar Cipher ")
    if (intcheck key)
        then putStrLn ("[*]")
        else putStrLn ("[x]")
    putStrLn ("Vigenere Cipher [*]")
    putStrLn ("Autokey Cipher [*]")
    let dkey = ceiling $ sqrt (fromIntegral $ length key)
    putStr ("Hill Cipher ")
    if (SMT.modmultinv (round $ SMF.determinant (SMT.sqrmatrix (take (dkey*dkey) (cycle key)) dkey)) 256) == Nothing
        then (putStrLn ("[x]"))
        else (putStrLn ("[*]"))
    putStr ("Double Layering ")
    if (elem '[' key) || (elem ']' key) || (elem ',' key) == True then putStrLn ("[x]") else putStrLn ("[*]")

intcheck :: String -> Bool
intcheck x =
    case reads x :: [(Integer, String)] of
      [(_,"")] -> True
      _ -> False
