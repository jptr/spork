#!/bin/sh
DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )
cd $DIR/..

cryptArray=(
    "caesar"
    "vigenere"
    "autokey"
    "hill"
    "railfence"
    "double[caesar,vigenere]"
    )

dirArray=(
    "caesar"
    "vigenere"
    "autokey"
    "hill"
    "railfence"
    "double"
    )

keyArray=(
    "15"
    "december"
    "RAPIDO"
    "howdy"
    "37"
    "[52,woah]"
    )

let w=0

for index in ${!cryptArray[*]}
do
    ./src/spork encrypt ${cryptArray[$index]} "$DIR/input/spork" --key="${keyArray[$index]}" --output="$DIR/tmp"
    if [[ $(cmp --silent "$DIR/output/${dirArray[$index]}/spork" "$DIR/tmp" && echo "success" || echo "failure") == "success" ]]
        then
            let w++
            printf "Binary: Encryption Test #%s passed. " $(( $index+1 ))
        else
            printf "Binary: Encryption Test #%s failed. " $(( $index+1 ))
        fi

    ./src/spork decrypt ${cryptArray[$index]} "$DIR/tmp" --key="${keyArray[$index]}"
    if [[ $(cmp --silent "$DIR/input/spork" "$DIR/tmp" && echo "success" || echo "failure") == "success" ]]
        then
            let w++
            printf "Decryption Test #%s passed.\n" $(( $index+1 ))
        else
            printf "Decryption Test #%s failed.\n" $(( $index+1 ))
        fi
done

let num=$(( 2*${#cryptArray[*]} ))

printf "%s out of %d Binary Tests passed \n" $w $num

