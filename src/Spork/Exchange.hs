--Exchange is a module centered around the keys the user will be using
--It was created to avoid dependency loops within spork

module Spork.Exchange where

import Data.List
import System.Directory

import qualified Spork.Bridge as SBR

--position identifies where a certain flag is within the array it is supplied

position :: String -> [String] -> Int
position cond pile =
    case findIndex (\x -> isInfixOf cond x) pile of
        Just n    -> n
        Nothing   -> -1

--grabkey resolves where and what spork should use as the encryption key

grabkey :: [String] -> IO String
grabkey options
    | kfile >= 0 = SBR.retrieve (drop 10 (options !! kfile))
    | kstr >= 0 = return $ drop 6 (options !! kstr)
    | otherwise = errorx "no key given"
        where {
            kfile = position "--keyfile=" options;
            kstr = position "--key=" options
        }

--forgekey takes the key the user is currently using
--and writes it to a file to create a keyfile

forgekey :: [String] -> IO ()
forgekey x = do
    name <- namegen "keyfile" 1
    putStrLn ("Key Saved to " ++ name)
    SBR.write name $ drop 6 (x !! position "--key" x)

namegen :: String -> Integer -> IO String
namegen x y = do
     a <- doesFileExist (x++(show y))
     if a == True then namegen x (succ y) else return (x++(show y))

errorx msg = errorWithoutStackTrace $ "error => " ++ msg
