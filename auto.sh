#!/bin/sh
DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )
cd $DIR
./test/text.sh
./test/binary.sh
./test/media.sh
rm test/tmp
