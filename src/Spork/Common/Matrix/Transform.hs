--Transform is a module focused on functions allowing the manipulation and "transformation"
--of vectors and matrices, essential for ciphers such as the Hill Cipher.

module Spork.Common.Matrix.Transform where

import Data.List
import Data.Char

import qualified Spork.Common.Batch as SCB
import qualified Spork.Common.Matrix.Factor as SMF

type Matrix a = [[a]]

inverse :: (Fractional a, Ord a) => Matrix a -> Matrix a
inverse mat = transpose [[x / (SMF.determinant mat) | x <- row ] | row <- (cofactor mat)]

determinant :: Num a => Matrix a -> a
determinant [] = errorx "0-by-0 matrix"
determinant [[n]] = n
determinant mat = sum [ (-1)^(j+1) * (head mat)!!(j-1) * determinant (remove mat 1 j) | j <- [1..(length $ head mat)]]

--produce the matrix of cofactors

cofactor :: (Fractional a, Ord a) => Matrix a -> Matrix a
cofactor mat = [[(\x y m -> (-1) ^ (x+y) * (SMF.determinant ((remove m x y)))) i j mat | j <- [1..n]] | i <- [1..n]]
    where n = length mat

--these functions allow you to cut out sections of the matrix (removing the ith row and jth column)

remove :: Matrix a -> Int -> Int -> Matrix a
remove mat i j
  | null mat || i < 1 || i > length mat || j < 1 || j > (length $ head mat) = errorx "(i,j) out of range"
  | otherwise = transpose $ slice (transpose $ slice mat i) j

slice :: [a] -> Int -> [a]
slice [] n = []
slice xs n
  | n < 1 || n > (length xs) = xs
  | otherwise = (take (n-1) xs) ++ drop n xs

--these functions were needed as a regular inverse does not work when modulos are applied
--so these use a method called the modular multiplicative inverse, which in this case
--is calculated with the extended euclidean algorithm.

modinverse :: (Integral a, Ord a) => Matrix a -> Matrix a
modinverse mat = SCB.mapmat round (transpose [[x * (fromIntegral (extract (modmultinv (round (SMF.determinant fracmat)) 256))) | x <- row] | row <- (cofactor fracmat)])
        where fracmat = SCB.mapmat fromIntegral mat

modmultinv :: Integral a => a -> a -> Maybe a
modmultinv num modval
  | 1 == g = Just (pos i)
  | otherwise = Nothing
  where {
    (i, _, g) = extea num modval;
    pos x
      | x < 0 = x + modval
      | otherwise = x
        }

extea :: Integral a => a -> a -> (a,a,a)
extea a 0 = (1, 0, a)
extea a b =
  let (q, r) = a `quotRem` b
      (s, t, g) = extea b r
  in (t, s - q * t, g)

extract :: Integral a => Maybe a -> a
extract (Just x) = x
extract Nothing = 0

--functions exclusively needed for formatting data into a matrix form

initial :: [Char] -> Int -> [Matrix Int]
initial cargo size
    | size <= 0 = errorx "negative or zero n"
    | interval /= 0 = (transpose [map (fromIntegral . ord) (take interval cargo)]):(chunk (drop interval cargo) size)
    | otherwise = chunk cargo size
        where interval = mod (length cargo) size

chunk :: [Char] -> Int -> [Matrix Int]
chunk [] _ = []
chunk cargo size = (transpose [map (fromIntegral . ord) (take size cargo)]) : (chunk (drop size cargo) size)

unchunk :: [Matrix Int] -> String
unchunk blocks = (map chr (concat $ concat blocks))

--marker and unmarker allow an easy way to get rid of null variables

marker :: [Matrix Int] -> [Matrix Int]
marker target
  | length filler /= length example = ((take (length example) (repeat [length example - length filler])) : (filler ++ (take (length example - length filler) (repeat [0]))) : (tail target))
  | otherwise = (take (length filler) (repeat [0])):target
      where {
        filler = head target;
        example = target!!1
            }

unmarker :: [Matrix Int] -> [Matrix Int]
unmarker target
    | loc == 0 = (tail target)
    | otherwise = (take ((length (target!!0)) - loc) (target!!1)) : (tail (tail target))
        where loc = head (head (head target))

--these functions allow a key to be generated
--not all matrices will have a modular multiplicative inverse
--so an error will be thrown when an inverse is not available

keygen :: (RealFrac a, Ord a) => String -> Matrix a
keygen pass
    | modmultinv (round (SMF.determinant key)) 256 == Nothing = errorx "no possible inverse, please try another key"
    | otherwise = key
    where {
        x = ceiling $ sqrt (fromIntegral $ length pass);
        key = (sqrmatrix (take (x*x) (cycle pass)) x)
          }

sqrmatrix :: (Fractional a, Ord a) => String -> Int -> Matrix a
sqrmatrix [] _ = []
sqrmatrix xs side = (take side (map (fromIntegral . ord) xs)):(sqrmatrix (drop side xs) side)

errorx msg = errorWithoutStackTrace $ "error => "++msg
